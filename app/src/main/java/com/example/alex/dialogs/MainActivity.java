package com.example.alex.dialogs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LeftFragment leftFragment = LeftFragment.getInstanse();
        RightFragment rightFragment = RightFragment.getInstanse();
        rightFragment.setInteractListener(leftFragment);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_left, leftFragment)
                .commit();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_right, rightFragment)
                .commit();
    }
}
