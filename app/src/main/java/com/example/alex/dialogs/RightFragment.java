package com.example.alex.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class RightFragment extends Fragment {

    public static RightFragment getInstanse(){
        return new RightFragment();
    }
    EditText editText;
    Button button;

    InteractListener interactListener;

    public void setInteractListener(InteractListener interactListener) {
        this.interactListener = interactListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_right, container, false);
        editText = root.findViewById(R.id.edit_text_view);
        button = root.findViewById(R.id.button_back);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(interactListener != null){
                    interactListener.show(editText.getText().toString());
                }
            }
        });
        return root;
    }


}
