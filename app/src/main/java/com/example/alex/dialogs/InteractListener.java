package com.example.alex.dialogs;

public interface InteractListener {

    void show(String text);
}
