package com.example.alex.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class LeftFragment extends Fragment implements InteractListener {


    public static LeftFragment getInstanse(){
        return new LeftFragment();
    }
    TextView label;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_left, container, false);
        label = root.findViewById(R.id.label_view);
        return root;
    }

    @Override
    public void show(String text) {
        label.setText(text);
    }
}
